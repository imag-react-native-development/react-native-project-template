import React from 'react';
import {
  StyleSheet,
  Text,
} from 'react-native';
import { setI18nLocale } from './utilities/localization/localization';

setI18nLocale('en');

const App = () => (
  <>
    <Text style={ styles.title }>
      Welcome to React Native App
    </Text>
  </>
);

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: '600',
    marginTop: '25%',
    textAlign: 'center',
  },
});

export default App;
