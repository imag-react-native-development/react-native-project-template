#!/bin/bash
set -e
# Color Variables
RED='\033[0;31m'
REDBOLD='\033[0;31m\033[1m'
BLUE='\033[0;34m'
BLUEBOLD='\033[0;34m\033[1m'
GREEN='\033[0;32m'
GREENBOLD='\033[0;32m\033[1m'
REDBG='\033[0;41m'
NC='\033[0m'

parentdir=$(dirname `pwd`)
currentdir=$PWD

read -p "Enter Package name/Bundle identifier (ex: com.company.appname): " appNameInput
IFS='.' read -ra nameSpace <<< "$appNameInput"
nameSpaceLength="${#nameSpace[@]}"
appName=${nameSpace[$nameSpaceLength-1]}

echo "${BLUEBOLD}Creating app in $parentdir/$appName folder"
echo "${GREENBOLD}react-native: 0.60"
echo "${NC}"
cd ..
npx react-native init "$appName" --version 0.60.0

echo "${BLUEBOLD}Creating folders...${NC}"
set +e
cd "$appName"
echo "${GREENBOLD}"
rm -rf "src" 2&> /dev/null
rm -rf "scripts" 2&> /dev/null
mkdir scripts
echo "-scripts"
mkdir src
echo "-src"
cd src
mkdir components
echo " └-components"
mkdir nagigators
echo " └-navigation"
mkdir screens
echo " └-screens"
mkdir services
echo " └-services"
mkdir state
echo " └-state"
cd state
mkdir actions
echo "   └-actions"
mkdir reducers
echo "   └-reducers"
mkdir sagas
echo "   └-sagas"
mkdir selectors
echo "   └-selectors"
mkdir types
echo "   └-types"
cd ..
mkdir theme
echo " └-theme"
mkdir utilities
echo " └-utilities"
cd ..
rm "index.js" 2&> /dev/null
rm ".env" 2&> /dev/null
rm ".env.dev" 2&> /dev/null
rm ".eslintrc.yaml" 2&> /dev/null
rm ".eslintrc.js" 2&> /dev/null
rm ".env.dev" 2&> /dev/null
rm -rf "__tests__" 2&> /dev/null

echo "${BLUEBOLD}Copying source files...${NC}"
cd "$currentdir"
cp "source/.env" "../${appName}/"
cp "source/.env.dev" "../${appName}/"
cp "source/.eslintrc.yaml" "../${appName}/"
cp "source/index.js" "../${appName}/"
cp "source/App.js" "../${appName}/src/"
cp "source/App.test.js" "../${appName}/src/"
cp "source/avd-helpers.sh" "../${appName}/scripts/"
cp "source/string.js" "../${appName}/src/utilities/"
cp "source/currency.js" "../${appName}/src/utilities/"
cp "source/date.js" "../${appName}/src/utilities/"
cp -r "source/localization" "../${appName}/src/utilities/"
cp "source/colors.js" "../${appName}/src/theme/"

set -e
cd ..
cd "$appName"
npm install -g npm-add-script --silent --no-progress --loglevel error
echo "${BLUEBOLD}Adding scripts to package.json...${NC}"
npmAddScript -k ios -v "ENVFILE=.env.dev react-native run-ios \${DEFAULT_IOS_SIMULATOR:+--simulator=\"\$DEFAULT_IOS_SIMULATOR\"}" -f
npmAddScript -k android -v  "source ./scripts/avd-helpers.sh && LaunchAvdIfNeeded && npm run android-device" -f
npmAddScript -k android-device -v "export ENV=dev; ENVFILE=.env.test react-native run-android && adb reverse tcp:9090 tcp:9090" -f


echo "${BLUEBOLD}Installing prop-types...${NC}"
npm install prop-types@^15.7 --save --silent --loglevel error

echo "${BLUEBOLD}Installing react-navigation...${NC}"
npm install react-navigation@^4.0 --save --silent --loglevel error

echo "${BLUEBOLD}Installing redux...${NC}"
npm install redux@^4.0 --save --silent --loglevel error

echo "${BLUEBOLD}Installing react-redux...${NC}"
npm install react-redux@^7.1 --save --silent --loglevel error

echo "${BLUEBOLD}Installing redux-devtools-extension...${NC}"
npm install redux-devtools-extension@^2.13 --save --silent --loglevel error

echo "${BLUEBOLD}Installing react-navigation-redux-helpers...${NC}"
npm install react-navigation-redux-helpers@^4.0 --save --silent --loglevel error

echo "${BLUEBOLD}Installing redux-form...${NC}"
npm install redux-form@^8.2 --save --silent --loglevel error

echo "${BLUEBOLD}Installing react-navigation...${NC}"
npm install react-navigation@^4.0 --save --silent --loglevel error

echo "${BLUEBOLD}Installing redux-saga...${NC}"
npm install redux-saga@^1.1 --save --silent --loglevel error

echo "${BLUEBOLD}Installing i18n-js...${NC}"
npm install i18n-js --save --silent --loglevel error

echo "${BLUEBOLD}Installing lodash...${NC}"
npm install lodash --save --silent --loglevel error

echo "${BLUEBOLD}Installing moment...${NC}"
npm install moment --save --silent --loglevel error

echo "${BLUEBOLD}Installing native-base...${NC}"
npm install native-base@^2.13 --save --silent --loglevel error

echo "${BLUEBOLD}Installing eslint...${NC}"
npm install babel-eslint eslint eslint-config-airbnb eslint-plugin-babel eslint-plugin-detox eslint-plugin-import --save-dev --silent --loglevel error
npm install eslint-plugin-jest eslint-plugin-jsx-a11y eslint-plugin-jsx-max-len eslint-plugin-react eslint-plugin-unicorn --save-dev --silent --loglevel error

echo "${BLUEBOLD}Installing test tools...${NC}"
npm install react-test-renderer redux-saga-test-plan redux-saga-tester --save-dev --silent --loglevel error

echo "${REDBOLD}Changing package name and bundle identifier...${NC}"
npx react-native-ci-tools bundle "$appNameInput" "$appName" -ia

echo "${NC}"
set +e
echo "Done"
