# React Native Project Template

Versions:
- React Native 0.60.0
- React Navigation ^4.0
- Redux ^4.0
- React Redux ^7.1
- Redux Form ^8.2
- Redux Saga ^1.1
- Native Base ^2.13


## Prerequisites

It requires Xcode or Android Studio to get started. If you already have one of these tools installed,
you should be able to get up and running within a few minutes. If they are not installed,
you should expect to spend about an hour installing and configuring them.

- Xcode
- Android Studio
- Node
- NPM

###Steps to create project template
- Check out this repository
- Run `sh react-native-app.sh`
- Enter package name/bundle identifier

